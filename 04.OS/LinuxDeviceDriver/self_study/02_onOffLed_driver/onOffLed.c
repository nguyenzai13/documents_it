#include <linux/module.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/interrupt.h>
#include <linux/ioctl.h>
#include <asm/gpio.h>
#include <asm/io.h>
#include <linux/err.h>

#define GPIO_ADDR_BASE 0x209C000
#define GPIO_SIZE 0x001801C
#define GPIO_DR_OFFSET 0x00
#define GPIO_GDIR_OFFSET 0x04
#define GPIO_PSR_OFFSET 0x08
#define GPIO_ICR1_OFFSET 0x0C
#define GPIO_ICR2_OFFSET 0x10
#define GPIO_IMR_OFFSET 0x14
#define GPIO_ISR_OFFSET 0x18
#define GPIO_EDGE_SEL_OFFSET 0x1C

#define IOMUXC_ADDR_BASE 0x20E4000
#define IOMUXC_SIZE 0x0087C
#define IOMUXC_DATA03_OFFSET 0x58
#define IOMUXC_DATA04_OFFSET 0x5C
#define IOMUXC_DATA05_OFFSET 0x60
#define IOMUXC_DATA06_OFFSET 0x64
#define IOMUXC_DATA07_OFFSET 0x68

#define ATL5 (5<<0)

#define GPIO_21 21

#define OUTPUT 1
#define INPUT 0
#define HIGH 1
#define LOW 0

#define GPIO_SET_BIT(x) (1<<x)
#define GPIO_CLEAR_BIT(x) ~(1<<x)
#define GPIO_CLEAR_DR 0

unsigned int __iomem *gpio_addr;
unsigned int __iomem *iomuxc_addr;

static void iomuxc_gpio_mode(int iomuxc_offset){
	iomuxc_offset /= 4;
	iomuxc_addr[iomuxc_offset] |= ATL5;
    printk("iomuxc_addr[iomuxc_offset]_%p \n",iomuxc_addr[iomuxc_offset]);
}

static void gpio_mode(int gpio, int mode){
	if(mode == INPUT){
		printk("gpio_%d INPUT\n", gpio);
		gpio_addr[GPIO_GDIR_OFFSET / 4] &= GPIO_CLEAR_BIT(gpio);
        printk("gpio_addr[GPIO_GDIR_OFFSET / 4]_%p \n",gpio_addr[GPIO_GDIR_OFFSET / 4]);
	}else if(mode == OUTPUT){
		printk("gpio_%d OUTPUT\n", gpio);
		gpio_addr[GPIO_GDIR_OFFSET / 4] |= GPIO_SET_BIT(gpio);
        printk("gpio_addr[GPIO_GDIR_OFFSET / 4]_%p \n",gpio_addr[GPIO_GDIR_OFFSET / 4]);
    }
}

static void gpio_write(int gpio, int level){
	if(level == HIGH){
		printk("gpio_%d HIGH\n", gpio);
		gpio_addr[GPIO_DR_OFFSET / 4] |= GPIO_SET_BIT(gpio);
         printk("gpio_addr[GPIO_DR_OFFSET / 4]_%p \n",gpio_addr[GPIO_DR_OFFSET / 4]);
	}else if(level == LOW){
		printk("gpio_%d LOW\n", gpio);
                gpio_addr[GPIO_DR_OFFSET / 4] &= GPIO_CLEAR_BIT(gpio);
         printk("gpio_addr[GPIO_DR_OFFSET / 4]_%p \n",gpio_addr[GPIO_DR_OFFSET / 4]);
	}
}

static int __init led_init(void)
{
	printk("Call init!\n");

	gpio_addr = ioremap(GPIO_ADDR_BASE, GPIO_SIZE);
	if(gpio_addr == NULL){
		printk("Mapping gpio_addr fail!\n");
	}
	iomuxc_addr = ioremap(IOMUXC_ADDR_BASE, IOMUXC_SIZE);
	if(iomuxc_addr == NULL){
                printk("Mapping iomuxc_addr fail!\n");
                goto iomuxc_addr_fail;
    }
    printk("gpio_addr_%p \n",gpio_addr);
    printk("iomuxc_addr_%p \n",iomuxc_addr);
	iomuxc_gpio_mode(GPIO_21);
	gpio_mode(GPIO_21, OUTPUT);
	gpio_write(GPIO_21, HIGH);

	return 0;
	
iomuxc_addr_fail:
	iounmap(gpio_addr);

	return -ENOMEM;
}

static void __exit led_exit(void)
{
	gpio_write(GPIO_21, LOW);
	iounmap(gpio_addr);
	iounmap(iomuxc_addr);
};

module_init(led_init);
module_exit(led_exit);

MODULE_LICENSE("GPL");
//MODULE_AUTHOR(AUTHOR);

