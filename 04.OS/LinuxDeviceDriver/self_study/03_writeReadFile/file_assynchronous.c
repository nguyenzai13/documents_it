//#include<stdio.h>
//#include<sys/types.h>
//#include<sys/stat.h>
//#include<fcntl.h>
//#include<unistd.h>
//#include<string.h>
//#include<aio.h>
//#include<errno.h>
#include <sys/types.h>
#include <aio.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

const int SIZE_TO_READ = 100;
int main()
{
	int file = open("myText.txt",O_RDONLY, 0);
	
	if (file == -1){
		printf("Cannot open file !\n");
		return 1;
	}
	
	//tao ra buffer
	char * buffer = (char*)calloc(SIZE_TO_READ, 1);

	//create callback structure
	struct aiocb cb;

	memset(&cb, 0, sizeof(struct aiocb));
	cb.aio_nbytes = SIZE_TO_READ; 
	cb.aio_fildes = file; //file can doc
	cb.aio_offset = 0; //doc tu vi tri nao trong file
	cb.aio_buf = buffer; //data duoc doc se luu vao buffer

	//read
	if(aio_read(&cb) == -1){
		printf("Unable to create request!\n");
		close(file);
	}

	printf("Request enqueued!\n");
	
	//wait until the request has finished
	while(aio_error(&cb) == EINPROGRESS)
	{
		printf("Still working ...\n");
	}

	//success?
	int numBytes = aio_return(&cb);

	if(numBytes != -1)
		printf("Success!\n");
	else
		printf("Error!\n");
	
	printf("buffer = %s\n",buffer);
	//now clean up
	free(buffer);
	close(file);

}

