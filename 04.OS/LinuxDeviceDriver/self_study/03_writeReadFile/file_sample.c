#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>

void main()
{
	int fd;
	fd = open("myText",O_CREAT|O_WRONLY);
	printf("fd1 = %d \n",fd);
	if(fd == -1)
		printf("Cannot open file !\n");
	int wr;
	wr = write(fd, "Hello world !", strlen("Hello world !"));
	if(wr == -1)
		printf("Cannot written to file !\n");	
	fsync(fd);
	close(fd);

	////

	fd = open("myText",O_CREAT|O_RDONLY);
    printf("fd2 = %d \n",fd);
    if(fd == -1)
        printf("Cannot open file !\n");
	int rd;
	char bf[14]="";
	rd = read(fd,bf,13);
	if(rd == -1)
		printf("Cannot read file !\n");
	bf[rd] = '\0';
	printf("rd = %d\n", rd);
	printf("bf = %s \n", bf);

	close(fd);
}
