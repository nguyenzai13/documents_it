#include<stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>
#include<stdlib.h>

void main()
{
	int fd = open("/dev/misc_device",O_CREAT|O_RDONLY);
	if(fd == -1){
		printf("Cannot open file !\n");

	}

	char buf[20];
	memset(buf, 0 , 20);
	
	int rd = read(fd, buf, 20);
	if(rd == -1){
		printf("cannot read file !\n");
	}
	printf("%s\n",buf);
	//buf[rd] = '\0';
	fsync(fd);
	close(fd);

}

/*
#define FILE    "/dev/misc_device"

int main(void)
{
        char buff[20];

        memset(buff, 0, 20);
        int fd = open(FILE, O_RDONLY);

        if (fd < 0) {
                perror("open\n");
                exit(1);
        }
        read(fd, buff, 20);

        printf("%s\n", buff);

        close(fd);
        return 0;
}
*/
