#include <linux/device.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/init.h>
#include <linux/reboot.h>
#include <linux/delay.h>
#include <linux/string.h>
#include <asm/gpio.h>
#include <asm/io.h>
#include <linux/err.h>
#include <linux/interrupt.h>
#include <linux/ioctl.h>

//Define for GPIO
#define GPIO_ADDR_BASE 0x209C000
#define GPIO_SIZE 0x001801C
#define GPIO_DR_OFFSET 0x00
#define GPIO_GDIR_OFFSET 0x04
#define GPIO_PSR_OFFSET 0x08
#define GPIO_ICR1_OFFSET 0x0C
#define GPIO_ICR2_OFFSET 0x10
#define GPIO_IMR_OFFSET 0x14
#define GPIO_ISR_OFFSET 0x18
#define GPIO_EDGE_SEL_OFFSET 0x1C

#define IOMUXC_ADDR_BASE 0x20E4000
#define IOMUXC_SIZE 0x0087C
#define IOMUXC_DATA03_OFFSET 0x58
#define IOMUXC_DATA04_OFFSET 0x5C
#define IOMUXC_DATA05_OFFSET 0x60
#define IOMUXC_DATA06_OFFSET 0x64
#define IOMUXC_DATA07_OFFSET 0x68

#define ATL5 (5<<0)

#define GPIO_21 20

#define OUTPUT 1
#define INPUT 0
#define HIGH 1
#define LOW 0

#define GPIO_SET_BIT(x) (1<<x)
#define GPIO_CLEAR_BIT(x) ~(1<<x)
#define GPIO_CLEAR_DR 0


unsigned int __iomem *gpio_addr;
unsigned int __iomem *iomuxc_addr;

static void iomuxc_gpio_mode(int iomuxc_offset){
        iomuxc_offset /= 4;
        iomuxc_addr[iomuxc_offset] |= ATL5;
    	printk("iomuxc_addr[iomuxc_offset]_%p \n",iomuxc_addr[iomuxc_offset]);
}

static void gpio_mode(int gpio, int mode){
        if(mode == INPUT){
                printk("gpio_%d INPUT\n", gpio);
                gpio_addr[GPIO_GDIR_OFFSET / 4] &= GPIO_CLEAR_BIT(gpio);
        	printk("gpio_addr[GPIO_GDIR_OFFSET / 4]_%p \n",gpio_addr[GPIO_GDIR_OFFSET / 4]);
        }else if(mode == OUTPUT){
                printk("gpio_%d OUTPUT\n", gpio);
                gpio_addr[GPIO_GDIR_OFFSET / 4] |= GPIO_SET_BIT(gpio);
        	printk("gpio_addr[GPIO_GDIR_OFFSET / 4]_%p \n",gpio_addr[GPIO_GDIR_OFFSET / 4]);
    	}	
}

static void gpio_write(int gpio, int level){
        if(level == HIGH){
                printk("gpio_%d HIGH\n", gpio);
                gpio_addr[GPIO_DR_OFFSET / 4] |= GPIO_SET_BIT(gpio);
         	printk("gpio_addr[GPIO_DR_OFFSET / 4]_%p \n",gpio_addr[GPIO_DR_OFFSET / 4]);
        }else if(level == LOW){
                printk("gpio_%d LOW\n", gpio);
                gpio_addr[GPIO_DR_OFFSET / 4] &= GPIO_CLEAR_BIT(gpio);
         	printk("gpio_addr[GPIO_DR_OFFSET / 4]_%p \n",gpio_addr[GPIO_DR_OFFSET / 4]);
        }
}
//End define for GPIO


//Create device file
#define DEVICE 1
#define DEVICE_NAME "led_kai"
#define AUTHOR          "Phu Luu An luuanphu@gmail.com"
#define DESC            "A example character device driver"

static char data[4096];
static dev_t dev_num;
char *buff = "kai nguyen";

static struct cdev my_cdev;
static struct class *class_name;
static struct device *device_name;


static int dev_open(struct inode *, struct file *);
static int dev_close(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char __user *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char __user *, size_t, loff_t *);	

static const struct file_operations fops = {
        .owner = THIS_MODULE,
        .open = dev_open,
	.release = dev_close,
	.read = dev_read,
	.write = dev_write,
};

static int dev_open(struct inode *inodep, struct file *filep)
{
	pr_info("open file\n");
	gpio_addr = ioremap(GPIO_ADDR_BASE, GPIO_SIZE);
        if(gpio_addr == NULL){
                printk("Mapping gpio_addr fail!\n");
        }
        iomuxc_addr = ioremap(IOMUXC_ADDR_BASE, IOMUXC_SIZE);
        if(iomuxc_addr == NULL){
                printk("Mapping iomuxc_addr fail!\n");
                goto iomuxc_addr_fail;
	}
    	printk("gpio_addr_%p \n",gpio_addr);
    	printk("iomuxc_addr_%p \n",iomuxc_addr);
        iomuxc_gpio_mode(GPIO_21);
        gpio_mode(GPIO_21, OUTPUT);	
	return 0;

iomuxc_addr_fail:
        iounmap(gpio_addr);

        return -ENOMEM;

}


static int dev_close(struct inode *inodep, struct file *filep)
{
	pr_info("close file\n");
	return 0;
}

static ssize_t dev_read(struct file *filep, char __user *buf, size_t len, loff_t *offset)
{
	int ret;
	pr_info("read file\n");
	ret = copy_to_user(buf, data, strlen(data));
	if (ret) {
		pr_alert("can not put to user\n");
		return ret;
	}
	pr_info("send %d letter to user\n", (int)strlen(data));
	return len;
}


static ssize_t dev_write(struct file *filep, const char __user *buf, size_t len, loff_t *offset)
{
	memset(data , 0 , 100);
	pr_info("write to file\n");
	copy_from_user(data, buf, len);
	char x[10] = "on\n";
	char y[10] = "off\n"; 	
	printk("write = %s \n", data);
	printk("writelen = %d \n", strlen(data));
	printk("x = %s \n", x);
	printk("xlen = %d \n", strlen(x));
	printk("strcmp = %d \n", strcmp(data,x));
	if(strcmp(data,x) == 0){
		gpio_write(GPIO_21, HIGH);
	}
	else if(strcmp(data,y) == 0){
		gpio_write(GPIO_21, LOW);
	}
	else{
		printk("Cannot control LED");
	}

	return len;
}

//End of create device file


//Init and Exit function
static int __init func_init(void)
{
	pr_info("init func\n");
	int ret;
	ret = alloc_chrdev_region(&dev_num, 0, 1, "hello");
	if(ret){
		printk("can not register major number\n");
		return ret;
	}
	printk("register sucessfully major now is: %d\n", MAJOR(dev_num));
	
	cdev_init(&my_cdev, &fops);
	my_cdev.owner = THIS_MODULE;	
	my_cdev.dev = dev_num;

	ret = cdev_add(&my_cdev, dev_num, 1);
	
	if(ret < 0){
		printk("cannot add");
		return ret;
	}
	
	class_name = class_create(THIS_MODULE, "hello");
	if(IS_ERR(class_name)){
		pr_info("create class failed\n");
		return PTR_ERR(class_name);
	}
	pr_info("create class success\n");
	
	device_name = device_create(class_name, NULL, dev_num, NULL, "ex_hello");
	if(IS_ERR(class_name)){
		pr_info("create device failed\n");
		return PTR_ERR(device_name);
	}
	pr_info("create device success\n");

	return 0;
}


static void __exit func_exit(void)
{
	pr_info("exit func\n");

        gpio_write(GPIO_21, LOW);
        iounmap(gpio_addr);
        iounmap(iomuxc_addr);

	cdev_del(&my_cdev);
	device_destroy(class_name, dev_num);
	class_destroy(class_name);
	unregister_chrdev(dev_num, DEVICE_NAME);
}

module_init(func_init);
module_exit(func_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR(AUTHOR);
MODULE_DESCRIPTION(DESC);
MODULE_VERSION("0.01");

