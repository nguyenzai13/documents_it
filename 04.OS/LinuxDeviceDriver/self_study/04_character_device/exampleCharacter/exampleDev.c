#include <linux/device.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/slab.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/init.h>
#include <linux/reboot.h>
#include <linux/delay.h>
#include <linux/string.h>


#define DEVICE 1
#define DEVICE_NAME "led_kai"
#define AUTHOR          "Phu Luu An luuanphu@gmail.com"
#define DESC            "A example character device driver"



static char data[4096];
static dev_t dev_num;
char *buff = "kai nguyen";

static struct cdev my_cdev;
static struct class *class_name;
static struct device *device_name;


static int dev_open(struct inode *, struct file *);
static int dev_close(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char __user *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char __user *, size_t, loff_t *);	

static const struct file_operations fops = {
        .owner = THIS_MODULE,
        .open = dev_open,
	.release = dev_close,
	.read = dev_read,
	.write = dev_write,
};

static int dev_open(struct inode *inodep, struct file *filep)
{
	pr_info("open file\n");
	return 0;
}


static int dev_close(struct inode *inodep, struct file *filep)
{
	pr_info("close file\n");
	return 0;
}

static ssize_t dev_read(struct file *filep, char __user *buf, size_t len, loff_t *offset)
{
	int ret;
	pr_info("read file\n");
	ret = copy_to_user(buf, data, strlen(data));
	if (ret) {
		pr_alert("can not put to user\n");
		return ret;
	}
	pr_info("send %d letter to user\n", (int)strlen(data));
	return len;
}


static ssize_t dev_write(struct file *filep, const char __user *buf, size_t len, loff_t *offset)
{
	memset(data , 0 , 100);
	pr_info("write to file\n");
	copy_from_user(data, buf, len);
	printk("write = %s", data);
	return len;
}


static int __init func_init(void)
{
	pr_info("init func\n");
	int ret;
	ret = alloc_chrdev_region(&dev_num, 0, 1, "hello");
	if(ret){
		printk("can not register major number\n");
		return ret;
	}
	printk("register sucessfully major now is: %d\n", MAJOR(dev_num));
	
	cdev_init(&my_cdev, &fops);
	my_cdev.owner = THIS_MODULE;	
	my_cdev.dev = dev_num;

	ret = cdev_add(&my_cdev, dev_num, 1);
	
	if(ret < 0){
		printk("cannot add");
		return ret;
	}
	
	class_name = class_create(THIS_MODULE, "hello");
	if(IS_ERR(class_name)){
		pr_info("create class failed\n");
		return PTR_ERR(class_name);
	}
	pr_info("create class success\n");
	
	device_name = device_create(class_name, NULL, dev_num, NULL, "ex_hello");
	if(IS_ERR(class_name)){
		pr_info("create device failed\n");
		return PTR_ERR(device_name);
	}
	pr_info("create device success\n");

	return 0;
}


static void __exit func_exit(void)
{
	pr_info("exit func\n");
	cdev_del(&my_cdev);
	device_destroy(class_name, dev_num);
	class_destroy(class_name);
	unregister_chrdev(dev_num, DEVICE_NAME);
}

module_init(func_init);
module_exit(func_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR(AUTHOR);
MODULE_DESCRIPTION(DESC);
MODULE_VERSION("0.01");

