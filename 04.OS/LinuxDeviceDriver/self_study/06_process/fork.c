#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int main()
{
	int a;
	pid_t cpid;
	cpid = fork();
	if(cpid == 0){
		printf("child pid = %d \n",getpid());
	}
	else{
		pid_t cpid2 = wait(&a);
		printf("parrent pid = %d \n",getpid());
		printf("a = %d \n", a);
		printf("cpid2 = %d\n", cpid2);
	}
	

}
