#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<unistd.h>
 
int main()
{
    int a = 0;
    int b = 0;
    pid_t cpid;
    if (fork()== 0){
	a = 10;
	return 5;
    }           /* terminate child */
    else{
	cpid = wait(&b); /* reaping parent */
    }
        
    printf("Parent pid = %d ,b = %d \n", getpid(),b);
    printf("Child pid = %d\n", cpid);
 
    return 0;
}
