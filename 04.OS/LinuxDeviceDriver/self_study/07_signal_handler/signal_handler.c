#include "stdio.h"
#include "signal.h"

void signal_handler(int signo){
	printf("hello world \n");
}

void main()
{
	sigset_t my_sigset;
	sigset_t old_sigset;
	sigemptyset(&my_sigset);
	sigaddset(&my_sigset, SIGINT);
	sigprocmask(SIG_BLOCK, &my_sigset, &old_sigset);
	while(1);
}

