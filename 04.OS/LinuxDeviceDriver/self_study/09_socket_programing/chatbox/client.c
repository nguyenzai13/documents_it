#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <errno.h>
#include <time.h>
#include <string.h>



int main()
{
	char data_send[1024] = "Nguyen Quoc Dai";
	char data_rec[1024] = "";
		
	int socket_client = -1;
	struct sockaddr_in socket_addr;
	
	//memset(data_send, 0, sizeof(data_send));
	memset(data_rec, 0, sizeof(data_rec));
	memset(&socket_addr, 0, sizeof(socket_addr));
	
	socket_client = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_client == -1)	printf("Create failed !!!\n");

	socket_addr.sin_family = AF_INET;
	socket_addr.sin_addr.s_addr = inet_addr("127.0.0.1");
	//socket_addr.sin_addr.s_addr = inet_addr("125.212.247.245");
	socket_addr.sin_port = htons(5000);
	
	//if( bind(socket_client,(struct sockaddr*)&socket_addr, sizeof(socket_addr)) == -1 ) 
	//	printf("Binding failed !!!\n");
	
	if( connect(socket_client,(struct sockaddr*)&socket_addr, sizeof(socket_addr)) == 0 ){
	
		//scanf("%s",data_send);
		write(socket_client, data_send, sizeof(data_send));
		//read(socket_client, data_rec, sizeof(data_rec));
		//printf("data receive from server %s\n", data_rec);

	}
	else
		printf("Cannot connect to server !!!\n");

	close(socket_client);

	return 0;
}
