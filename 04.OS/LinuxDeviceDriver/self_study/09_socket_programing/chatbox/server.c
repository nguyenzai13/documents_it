#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <string.h>
#include <errno.h>
#include <time.h>

int main()
{
	char data_server[1024];
	char data_rec[1024];
		
	int socket_listen;
	int socket_connect;
	int socket_receive;
	struct sockaddr_in socket_server;

	memset(data_rec, 0, sizeof(data_rec));
	memset(data_server, 0, sizeof(data_server));
	memset(&socket_server, 0, sizeof(socket_server));

	socket_listen = socket(AF_INET, SOCK_STREAM, 0);
	if(socket_listen == -1)	printf("Create failed !!!\n");

	socket_server.sin_family = AF_INET;
	socket_server.sin_addr.s_addr = inet_addr("127.0.0.1");
	socket_server.sin_port = htons(5000);
	
	if( bind(socket_listen,(struct sockaddr*)&socket_server, sizeof(socket_server)) == -1 ) 
		printf("Binding failed !!!\n");
	
	listen(socket_listen, 10);	

	while(1){
		socket_connect = accept(socket_listen, NULL, NULL);
		fgets(data_server, 1024, stdin);
		write(socket_connect, data_server, sizeof(data_server));
		close(socket_connect); 
	
		//socket_receive = accept(socket_listen, NULL, NULL);
		//read(socket_receive, data_rec, sizeof(data_rec));
		//printf("Data send from client: %s\n", data_rec);
		//close(socket_receive); 
	}
	close(socket_listen);

	return 0;
}
